# Kompact-5 Injector

Diverse x64 DLL Injector for Windows.

Kompact-5 attempts to bypass common userland AV/EDR hooks with vague argumentation. No procedural addresses are requested from foreign DLLs/PEs, and System DLL procedures are requested by ordinal to bypass basic run-time function black-listing. 

Kompact-5 has extensive support for kernel-land functions to bypass developer-implemented user-land hooks. These kernal-land functions are requested from NTDLL, and the corresponding procedures are also requested by ordinal. 

# Disclaimer: 
Deep work-in-progress. Debug/testing code will be published until an official release is imminent. 
